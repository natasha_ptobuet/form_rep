/**
 * Created by Наталья on 26.08.2016.
 */
$(document).ready(function(){
    var checkPeremBirth=true;  var checkPeremBirth2=true;
    var checkPeremSurname=true; var checkPeremName=true;
    var checkPeremPatron1=true; var checkPeremPatron2=true;
    var checkPeremPhone=true; var checkPeremAdress=true;
    var checkPeremNumber=true;

    var jVal = {
        'surname' : function() {
            $('body').append('<div id="surnameInfo" class="info"></div>');
            var nameInfo = $('#surnameInfo');
            var ele = $('#surname');
            var patt = /^[а-яА-Яё]+$/i;
            var pos = ele.offset();
            nameInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#patronymic');
                if(checkPeremSurname){
            move.offset(function(i,val){

                    checkPeremSurname=false;
                    return {top:val.top +7, left:val.left};
                });}
                nameInfo.removeClass('correct').addClass('error').html('Вы неверно ввели фамилию').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( nameInfo.hasClass("error") ) {
                var move=$('#patronymic');
                move.offset(function(i,val){
                    return {top:val.top-7, left:val.left};});
                nameInfo.removeClass('correct').addClass('error').html('Вы неверно ввели фамилию').hide()
                nameInfo.removeClass('error');

            }
        },
        'name' : function() {
            $('body').append('<div id="nameInfo" class="info"></div>');
            var nameInfo = $('#nameInfo');
            var ele = $('#name');
            var patt = /^[а-яА-Яё]+$/i;
            var pos = ele.offset();
            nameInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#birthday');
                if(checkPeremName){
                move.offset(function(i,val){

                        checkPeremName=false;
                    return {top:val.top +8, left:val.left};
                });}
                nameInfo.removeClass('correct').addClass('error').html('Вы неверно ввели имя').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( nameInfo.hasClass("error") ) {
                var move=$('#birthday');
                move.offset(function(i,val){
                    return {top:val.top-8, left:val.left};});
                nameInfo.removeClass('correct').addClass('error').html('Вы неверно ввели имя').hide()
                nameInfo.removeClass('error');
                checkPeremName=true;

            }
        },

        'patronymic' : function() {
            $('body').append('<div id="patronymInfo" class="info"></div>');
            var patronymInfo = $('#patronymInfo');
            var ele = $('#patronymic');
            var patt = /^[а-яА-Яё]+$/i;
            var pos = ele.offset();
            patronymInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#phone');
                if(checkPeremPatron1){
                move.offset(function(i,val){

                        checkPeremPatron1=false;
                    return {top:val.top +12, left:val.left};
                });}
                var move2=$('#span1');
                if(checkPeremPatron2){
                    checkPeremPatron2=false;
                move2.offset(function(i,val){

                    return {top:val.top +12, left:val.left};
                });}
                patronymInfo.removeClass('correct').addClass('error').html('Вы неверно ввели отчество').show();

            }
            else if ( patronymInfo.hasClass("error") ) {
                var move=$('#phone');
                move.offset(function(i,val){
                    return {top:val.top-12, left:val.left};});
                var move2=$('#span1');
                move2.offset(function(i,val){
                    return {top:val.top -12, left:val.left};
                });
                patronymInfo.removeClass('correct').addClass('error').html('Вы неверно ввели отчество').hide()
              patronymInfo.removeClass('error');
                checkPeremPatron1=true;
                checkPeremPatron2=true;

            }
        },

        'birthday' : function() {
            $('body').append('<div id="birthInfo" class="info"></div>');
            var birthInfo = $('#birthInfo');
            var ele = $('#birthday');
            var patt = /(\d{1,2}).(\d{1,2}).(\d{4})/;
            var pos = ele.offset();
            birthInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#adress');
                if(checkPeremBirth){
                move.offset(function(i,val){

                        checkPeremBirth=false;
                    return {top:val.top +12, left:val.left};

                });}
                var move2=$('#span2');
                if(checkPeremBirth2){
                    checkPeremBirth2=false;
                    move2.offset(function(i,val){

                        return {top:val.top +12, left:val.left};
                    });}
                birthInfo.removeClass('correct').addClass('error').html('Вы неверно ввели дату рождения').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( birthInfo.hasClass("error") ) {
                var move=$('#adress');
                move.offset(function(i,val){
                    return {top:val.top-12, left:val.left};});
                var move2=$('#span2');
                move2.offset(function(i,val){
                    return {top:val.top -12, left:val.left};
                });
                birthInfo.removeClass('correct').addClass('error').html('Вы неверно ввели дату рождения').hide()
                birthInfo.removeClass('error');
                checkPeremBirth2=true;
                checkPeremBirth=true;

            }
        },
        'phone' : function() {
            $('body').append('<div id="phoneInfo" class="info"></div>');
            var phoneInfo = $('#phoneInfo');
            var ele = $('#phone');
            var patt = (/^[0-9+]{1}[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}/);

            var pos = ele.offset();
            phoneInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#span1');
                if(checkPeremPhone){
                    move.offset(function(i,val){

                        checkPeremPhone=false;
                        return {top:val.top +13, left:val.left};
                    });}
                phoneInfo.removeClass('correct').addClass('error').html('Вы неверно ввели имя').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( phoneInfo.hasClass("error") ) {
                var move=$('#span1');
                move.offset(function(i,val){
                    return {top:val.top-13, left:val.left};});
                phoneInfo.removeClass('correct').addClass('error').html('Вы неверно ввели имя').hide()
                phoneInfo.removeClass('error');
                checkPeremPhone=true;

            }
        },
        'adress' : function() {
            $('body').append('<div id="adressInfo" class="info"></div>');
            var adressInfo = $('#adressInfo');
            var ele = $('#adress');
            var patt = /^[а-яА-Яё]/i;

            var pos = ele.offset();
            adressInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#span2');
                if(checkPeremAdress){
                    move.offset(function(i,val){

                        checkPeremAdress=false;
                        return {top:val.top +13, left:val.left};
                    });}
                adressInfo.removeClass('correct').addClass('error').html('Вы неверно ввели адрес').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( adressInfo.hasClass("error") ) {
                var move=$('#span2');
                move.offset(function(i,val){
                    return {top:val.top-13, left:val.left};});
                adressInfo.removeClass('correct').addClass('error').html('Вы неверно ввели адрес').hide()
                adressInfo.removeClass('error');
                checkPeremAdress=true;

            }
        },
        'email' : function() {
            $('body').append('<div id="emailInfo" class="info"></div>');
            var emailInfo = $('#emailInfo');
            var ele = $('#email');
            var patt = (/^[+a-zA-Z0-9-_]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);

            var pos = ele.offset();
            emailInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;

                emailInfo.removeClass('correct').addClass('error').html('Вы неверно ввели email').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( emailInfo.hasClass("error") ) {

                emailInfo.removeClass('correct').addClass('error').html('Вы неверно ввели email').hide()
                emailInfo.removeClass('error');


            }
        },
        'number_cart' : function() {
            $('body').append('<div id="numberInfo" class="info"></div>');
            var numberInfo = $('#numberInfo');
            var ele = $('#number_cart');
            var patt = /^[0-9-_./]+$/;

            var pos = ele.offset();
            numberInfo.css({
                top: pos.top+65,
                left: pos.left

            });
            if((ele.val().length < 2)|(!patt.test(ele.val()))) {
                jVal.errors = true;
                var move=$('#span3');
                if(checkPeremNumber){
                    move.offset(function(i,val){

                        checkPeremNumber=false;
                        return {top:val.top +13, left:val.left};
                    });}
                numberInfo.removeClass('correct').addClass('error').html('Вы неверно ввели имя').show();
                ele.removeClass('normal').addClass('wrong');
            }
            else if ( numberInfo.hasClass("error") ) {
                var move=$('#span3');
                move.offset(function(i,val){
                    return {top:val.top-13, left:val.left};});
               numberInfo.removeClass('correct').addClass('error').html('Вы неверно ввели имя').hide()
                numberInfo.removeClass('error');
                checkPeremNumber=true;


            }
        },
        'sendIt' : function (){
            if(!jVal.errors) {
                alert("отпраавили");
                $('#patient_form').submit();

            }
        }
    };
            $('#submit').click(function (){

                var obj =  $('body');
                obj.animate({ scrollTop: $('#patient_form').offset().top }, 750, function (){
                    jVal.errors = false;
                    jVal.surname();
                    jVal.name();
                    jVal.patronymic();
                    jVal.birthday();
                    jVal.phone();
                    jVal.adress();
                    jVal.email();
                    jVal.number_cart();
                    jVal.sendIt();
                });
                return false;
            });








    // bind jVal.fullName function to "Full name" form field
    $('#surname').change(jVal.surname);
    $('#name').change(jVal.name);
    $('#patronymic').change(jVal.patronymic);
    $('#birthday').change(jVal.birthday);
    $('#phone').change(jVal.phone);
    $('#adress').change(jVal.adress);
    $('#email').change(jVal.email);
    $('#number_cart').change(jVal.number_cart);


});